Feature: Amenagement de site - Interface ADDOO
[ADDOO]Afin de repondre a la demande du client
[ADDOO]En tant qu'utilisateur final reel
[ADDOO]Je veux avoir la possibilite d'amenager un site tout en prenant en compte les contraintes metiers

#Pré video : login.
Scenario: [ADDOO]Acceder a ADDOO avec des informations valides
Given [ADDOO]Je suis sur la page d'authentification
When [ADDOO]Je remplis l'UPR avec UPR Sud-Ouest et l'utilisateur
And [ADDOO]Je clique sur le bouton Acceder a ADDOO
Then [ADDOO]Je devrais acceder a l'interface ADDOO avec le bon UPR et utilisateur

#0 à 2 minutes
Scenario: [ADDOO]Ajouter une demande d'amenagement sur l'application ADDOO
Given [ADDOO]Je me trouve dans le sous-onglet Details demande de l'onglet demande sur l'interface ADDOO
When [ADDOO]Je renseigne le nom de la demande et le DR
And [ADDOO]Je remplis les champs necessaires dans la partie Operation
And [ADDOO]Je remplis les champs necessaires dans la partie Localisation/MKT
And [ADDOO]Je remplis les champs necessaires dans la partie Details GSM
And [ADDOO]je clique sur le bouton Ajouter
Then [ADDOO]Le numero de la demande apparait 
And [ADDOO]Des champs de la partie localisation/MKT apparaissent avec un fond bleu clair

#2 à 2:13 minutes
Scenario: [ADDOO]Valider l'UPR de la demande d'amenagement creee
Given [ADDOO]Une demande d'amenagement a ete creee avec un UPR specifique
When [ADDOO]J'ai fini de verifier l'UPR rentree par l'utilisateur
And [ADDOO]Je clique sur le bouton Valider UPR
Then [ADDOO]La checkbox Valider UPR de la partie operation a ete cochee

#2:13 à 2:35 minutes
Scenario: [ADDOO]Valider le siege de la demande d'amenagement creee
Given [ADDOO]Une demande d'amenagement a ete creee et l'utilisateur vient de valider l'UPR
When [ADDOO]J'ai fini de verifier le siege rentree par l'utilisateur
And [ADDOO]Je clique sur le bouton Valider siege
Then [ADDOO]La checkbox Valider siege de la partie operation a ete cochee

#2:45 à 4:50 minutes
Scenario: [ADDOO]Traiter une demande en erreur
Given [ADDOO]Une demande de la part de ADDOO a ete rejetee par NORIA
And [ADDOO]Un message d erreur est indique dans l onglet Demandes en erreur
Then [ADDOO]Je change les valeurs correspondantes sur l'interface de demande pour traiter l erreur
Then [ADDOO]J enregistre les nouvelles valeurs en cliquant sur Enregistrer
And [ADDOO]J annule la validation de siege
And [ADDOO]J annule la validation d UPR
And [ADDOO]Je valide de nouveau l UPR
And [ADDOO]Je valide de nouveau le siege
Then [ADDOO]La demande ne doit plus etre en erreur

