Feature: Amenagement de site - Interface NEW BODEGA
[NEW BODEGA]Afin de repondre a la demande du client
[NEW BODEGA]En tant qu'utilisateur final reel
[NEW BODEGA]Je veux avoir la possibilite d'amenager un site tout en prenant en compte les contraintes metiers

#Jusqu'à 8:50 minutes
Scenario: [NEW BODEGA]Acceder a NEW BODEGA et rechercher une demande ADDOO
Given [NEW BODEGA]Je suis sur la page d'accueil
When [NEW BODEGA]J'accede a la partie recherche globale en cliquant sur l'onglet Projets
And [NEW BODEGA]Je recherche une demande a partir de son numero d'identification defini dans ADDOO lors de sa creation
And [NEW BODEGA]Je clique sur le bouton rechercher
And [NEW BODEGA]Je clique sur la ligne correspondante a notre amenagement de site
Then [NEW BODEGA]L'utilisateur accede a une nouvelle page affichant les informations de l'amenagement

#8:50 à 9:55 minutes
Scenario: [NEW BODEGA]Valider un amenagement
Given [NEW BODEGA]Je suis sur la page d information sur l amenagement
Then [NEW BODEGA]Je clique sur statut et accede à un encadre avec menu deroulant
And [NEW BODEGA]Je selectionne en cours sur dans le menu deroulant
And [NEW BODEGA]Une ligne mise en visibilite apparait dans la categorie a faire
Then [NEW BODEGA]Je clique sur le carnet et un encadre mise en visibilite apparait
Then [NEW BODEGA]Je clique sur reelle dans la checkbox et je clique sur indiquer
And [NEW BODEGA]Mise en visibilite est remplace par affectation cdp
Then [NEW BODEGA]Je clique sur le carnet et un encadre affectation cdp apparait
And [NEW BODEGA]Je clique reelle dans la checkbox et valide avec indiquer
Then [NEW BODEGA]Un encadre dep nego apparait
And [NEW BODEGA]Je clique sur indiquer
Then [NEW BODEGA]Affectation cdp est devenu affectation moe
Then [NEW BODEGA]Je clique sur le carnet et un encadre affectation moe apparait
And [NEW BODEGA]Je clique reelle dans la checkbox et je clique sur indiquer
Then [NEW BODEGA]Un encadre moe nego apparait
And [NEW BODEGA]Je selectionne axians dans le menu deroulant et clique sur indiquer
Then [NEW BODEGA]J accede a la page commandes amenagements

#9:55 à 11:15 minutes
Scenario: [NEW BODEGA]Modifier les valeurs d un amenagement
Given [NEW BODEGA]Je suis sur la page commandes amenagements
Then [NEW BODEGA]Je remplis les champs de cout lot un sur deux etape un
Then [NEW BODEGA]Je clique sur creer document
And [NEW BODEGA]Les champs de cout lot un sur deux etape un sont verouilles
Then [NEW BODEGA]J accede a la page des details d amenagement
And [NEW BODEGA]Je clique sur le carnet de sitepot
And [NEW BODEGA]J accede a la page sitepot amenagement
Then [NEW BODEGA]Ajouter une addresse et des coordonnees dans identification candidat
Then [NEW BODEGA]Je clique sur creer document

#11:20 à 11:49 minutes
Scenario: [NEW BODEGA] Mettre en visibilite un amenagement en validant
Given [NEW BODEGA]Je suis sur sitepot amenagement
Then [NEW BODEGA]Je clique sur mise en visibilite
And [NEW BODEGA]Un encadre remarque concernant la validation apparait
Then [NEW BODEGA]Je clique sur confirmer validation et suis de retour sur la page sitepot amenagement
Then [NEW BODEGA]Je clique sur valider et accede a l encadre remarque concernant la validation
And [NEW BODEGA]Je clique sur confirmer validation et suis de retour sur la page sitepot amenagement
Then [NEW BODEGA]Je clique sur valider radio et accede a l encadre remarque concernant la validation
And [NEW BODEGA]Je clique sur confirmer validation et suis de retour sur la page sitepot amenagement
Then [NEW BODEGA]Je clique sur details candidat et accede a la page des details de l amenagement

#11:55 à 13:12 minutes
Scenario: [NEW BODEGA]Ajout d un candidat
Given [NEW BODEGA]Je suis sur la page de mon amenagement
Then [NEW BODEGA]Je clique sur ajouter candidat et accede a la page sitepot amenagement
And [NEW BODEGA]Je remplis les informations de identification candidat
Then [NEW BODEGA]Je clique sur creer document et accede a sitepot amenagement
Then [NEW BODEGA]Je clique sur mise en visibilite sitepot et ouvre l encadre remarque concernant la validation
Given [NEW BODEGA]Je suis de retour sur la page sitepot amenagement
Then [NEW BODEGA]Je clique sur valider et accede a l encadre remarque concernant la validation
And [NEW BODEGA]Je clique sur confirmer validation et suis de retour sur la page sitepot amenagement
Then [NEW BODEGA]Je clique sur valider radio et accede a l encadre remarque concernant la validation
And [NEW BODEGA]Je clique sur confirmer validation et suis de retour sur la page sitepot amenagement
Then [NEW BODEGA]Je clique sur details candidat et accede a la page des details de l amenagement



