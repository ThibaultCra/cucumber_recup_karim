$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/java/features/AmenagementSiteADDOO.feature");
formatter.feature({
  "line": 1,
  "name": "Amenagement de site - Interface ADDOO",
  "description": "[ADDOO]Afin de repondre a la demande du client\r\n[ADDOO]En tant qu\u0027utilisateur final reel\r\n[ADDOO]Je veux avoir la possibilite d\u0027amenager un site tout en prenant en compte les contraintes metiers",
  "id": "amenagement-de-site---interface-addoo",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 7,
  "name": "[ADDOO]Acceder a ADDOO avec des informations valides",
  "description": "",
  "id": "amenagement-de-site---interface-addoo;[addoo]acceder-a-addoo-avec-des-informations-valides",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 8,
  "name": "[ADDOO]Je suis sur la page d\u0027authentification",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "[ADDOO]Je remplis l\u0027UPR avec UPR Sud-Ouest et l\u0027utilisateur",
  "keyword": "When "
});
formatter.step({
  "line": 10,
  "name": "[ADDOO]Je clique sur le bouton Acceder a ADDOO",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "[ADDOO]Je devrais acceder a l\u0027interface ADDOO avec le bon UPR et utilisateur",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDefs_AmenagementSiteADDOO.je_suis_sur_la_page_d_authentification()"
});
formatter.result({
  "duration": 8132564699,
  "status": "passed"
});
formatter.match({
  "location": "StepDefs_AmenagementSiteADDOO.addoo_Je_remplis_l_UPR_avec_UPR_Sud_Ouest_et_l_utilisateur()"
});
formatter.result({
  "duration": 8375351830,
  "status": "passed"
});
formatter.match({
  "location": "StepDefs_AmenagementSiteADDOO.addoo_Je_clique_sur_le_bouton_Acceder_a_ADDOO()"
});
formatter.result({
  "duration": 16764,
  "status": "passed"
});
formatter.match({
  "location": "StepDefs_AmenagementSiteADDOO.je_devrais_acceder_a_l_interface_ADDOO_avec_le_bon_UPR_et_utilisateur()"
});
formatter.result({
  "duration": 74240,
  "status": "passed"
});
formatter.scenario({
  "line": 14,
  "name": "[ADDOO]Ajouter une demande d\u0027amenagement sur l\u0027application ADDOO",
  "description": "",
  "id": "amenagement-de-site---interface-addoo;[addoo]ajouter-une-demande-d\u0027amenagement-sur-l\u0027application-addoo",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 15,
  "name": "[ADDOO]Je me trouve dans le sous-onglet Details demande de l\u0027onglet demande sur l\u0027interface ADDOO",
  "keyword": "Given "
});
formatter.step({
  "line": 16,
  "name": "[ADDOO]Je renseigne le nom de la demande et le DR",
  "keyword": "When "
});
formatter.step({
  "line": 17,
  "name": "[ADDOO]Je remplis les champs necessaires dans la partie Operation",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "[ADDOO]Je remplis les champs necessaires dans la partie Localisation/MKT",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "[ADDOO]Je remplis les champs necessaires dans la partie Details GSM",
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "[ADDOO]je clique sur le bouton Ajouter",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "[ADDOO]Le numero de la demande apparait",
  "keyword": "Then "
});
formatter.step({
  "line": 22,
  "name": "[ADDOO]Des champs de la partie localisation/MKT apparaissent avec un fond bleu clair",
  "keyword": "And "
});
formatter.match({
  "location": "StepDefs_AmenagementSiteADDOO.je_me_trouve_dans_le_sous_onglet_Details_demande_de_l_onglet_demande_sur_l_interface_ADDOO()"
});
formatter.result({
  "duration": 269250,
  "status": "passed"
});
formatter.match({
  "location": "StepDefs_AmenagementSiteADDOO.je_renseigne_le_nom_de_la_demande_et_le_DR()"
});
formatter.result({
  "duration": 18475,
  "status": "passed"
});
formatter.match({
  "location": "StepDefs_AmenagementSiteADDOO.je_remplis_les_champs_necessaires_dans_la_partie_Operation()"
});
formatter.result({
  "duration": 16764,
  "status": "passed"
});
formatter.match({
  "location": "StepDefs_AmenagementSiteADDOO.je_remplis_les_champs_necessaires_dans_la_partie_Localisation_MKT()"
});
formatter.result({
  "duration": 16080,
  "status": "passed"
});
formatter.match({
  "location": "StepDefs_AmenagementSiteADDOO.je_remplis_les_champs_necessaires_dans_la_partie_Details_GSM()"
});
formatter.result({
  "duration": 15395,
  "status": "passed"
});
formatter.match({
  "location": "StepDefs_AmenagementSiteADDOO.je_clique_sur_le_bouton_Ajouter()"
});
formatter.result({
  "duration": 13343,
  "status": "passed"
});
formatter.match({
  "location": "StepDefs_AmenagementSiteADDOO.le_numero_de_la_demande_apparait()"
});
formatter.result({
  "duration": 13001,
  "status": "passed"
});
formatter.match({
  "location": "StepDefs_AmenagementSiteADDOO.des_champs_de_la_partie_localisation_MKT_apparaissent_avec_un_fond_bleu_clair()"
});
formatter.result({
  "duration": 17448,
  "status": "passed"
});
formatter.scenario({
  "line": 25,
  "name": "[ADDOO]Valider l\u0027UPR de la demande d\u0027amenagement creee",
  "description": "",
  "id": "amenagement-de-site---interface-addoo;[addoo]valider-l\u0027upr-de-la-demande-d\u0027amenagement-creee",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 26,
  "name": "[ADDOO]Une demande d\u0027amenagement a ete creee avec un UPR specifique",
  "keyword": "Given "
});
formatter.step({
  "line": 27,
  "name": "[ADDOO]J\u0027ai fini de verifier l\u0027UPR rentree par l\u0027utilisateur",
  "keyword": "When "
});
formatter.step({
  "line": 28,
  "name": "[ADDOO]Je clique sur le bouton Valider UPR",
  "keyword": "And "
});
formatter.step({
  "line": 29,
  "name": "[ADDOO]La checkbox Valider UPR de la partie operation a ete cochee",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDefs_AmenagementSiteADDOO.une_demande_d_amenagement_a_ete_creee_avec_un_UPR_specifique()"
});
formatter.result({
  "duration": 39344,
  "status": "passed"
});
formatter.match({
  "location": "StepDefs_AmenagementSiteADDOO.j_ai_fini_de_verifier_l_UPR_rentree_par_l_utilisateur()"
});
formatter.result({
  "duration": 23607,
  "status": "passed"
});
formatter.match({
  "location": "StepDefs_AmenagementSiteADDOO.je_clique_sur_le_bouton_Valider_UPR()"
});
formatter.result({
  "duration": 21554,
  "status": "passed"
});
formatter.match({
  "location": "StepDefs_AmenagementSiteADDOO.la_checkbox_Valider_UPR_de_la_partie_operation_a_ete_cochee()"
});
formatter.result({
  "duration": 18817,
  "status": "passed"
});
formatter.scenario({
  "line": 32,
  "name": "[ADDOO]Valider le siege de la demande d\u0027amenagement creee",
  "description": "",
  "id": "amenagement-de-site---interface-addoo;[addoo]valider-le-siege-de-la-demande-d\u0027amenagement-creee",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 33,
  "name": "[ADDOO]Une demande d\u0027amenagement a ete creee et l\u0027utilisateur vient de valider l\u0027UPR",
  "keyword": "Given "
});
formatter.step({
  "line": 34,
  "name": "[ADDOO]J\u0027ai fini de verifier le siege rentree par l\u0027utilisateur",
  "keyword": "When "
});
formatter.step({
  "line": 35,
  "name": "[ADDOO]Je clique sur le bouton Valider siege",
  "keyword": "And "
});
formatter.step({
  "line": 36,
  "name": "[ADDOO]La checkbox Valider siege de la partie operation a ete cochee",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDefs_AmenagementSiteADDOO.une_demande_d_amenagement_a_ete_creee_et_l_utilisateur_vient_de_valider_l_UPR()"
});
formatter.result({
  "duration": 38317,
  "status": "passed"
});
formatter.match({
  "location": "StepDefs_AmenagementSiteADDOO.j_ai_fini_de_verifier_le_siege_rentree_par_l_utilisateur()"
});
formatter.result({
  "duration": 19159,
  "status": "passed"
});
formatter.match({
  "location": "StepDefs_AmenagementSiteADDOO.addoo_Je_clique_sur_le_bouton_Valider_siege()"
});
formatter.result({
  "duration": 19501,
  "status": "passed"
});
formatter.match({
  "location": "StepDefs_AmenagementSiteADDOO.addoo_La_checkbox_Valider_siege_de_la_partie_operation_a_ete_cochee()"
});
formatter.result({
  "duration": 19159,
  "status": "passed"
});
});