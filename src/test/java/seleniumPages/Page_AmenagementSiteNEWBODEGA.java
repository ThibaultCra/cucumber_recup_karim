package seleniumPages;

import java.awt.AWTException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import common.Page_BasePage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class Page_AmenagementSiteNEWBODEGA extends Page_BasePage {

	public static final String GASSI_URL = "https://gassi.si.francetelecom.fr/";
	 Actions builder;
	
	public Page_AmenagementSiteNEWBODEGA() {
		System.setProperty("webdriver.chrome.driver","C:\\Users\\A738696\\Desktop\\automated tests\\chromedriver_win32\\chromedriver.exe");
	}
	
	
	public void launchBrowser() {
		driver = new ChromeDriver();
	//	driver.manage().window().maximize();
		System.out.println("Browser launched");
	}
	
	
	public void openNewBodegaInterface() {
		driver.get(GASSI_URL);
		driver.manage().window().maximize();
		System.out.println("[ADOO]User connected to the following URL: " + GASSI_URL);
		
		
		/* se connecter au site gassi */
		WebDriverWait wait = new WebDriverWait(driver, 10);
		//Find frame and switch
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("bgmainframe"));

		//Now find the element 
		WebElement user_input = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("user")));
		user_input.sendKeys("PGQG1130");
		
		WebElement password_input = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("password")));
		password_input.sendKeys("pgqg1130");
		
		//cliquer sur le bouton sign in afin de valider la saisie
		WebElement element = driver.findElement(By.id("spanLinkValidForm"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);

	
		
		new WebDriverWait(driver, 20).until(ExpectedConditions
		.presenceOfElementLocated(By.xpath("//*[@id=\"tabapplis\"]/tbody/tr[6]/td[2]/a"))).click();

		//Once all your stuff done with this frame need to switch back to default
		driver.switchTo().defaultContent();
		
		//switch de vue( nouvelle window, noria ici)
		// Switch to new window opened
		
		// Switch to new window opened
		for(String winHandle : driver.getWindowHandles()){
			
		    driver.switchTo().window(winHandle);
		    System.out.println("abcdef " +  driver.getTitle());
		    if (driver.getTitle().equals("ClickOnSite"))
		    	break;
		}
		
		
		
		System.out.println("okkkkkkkkkkkk " +  driver.getTitle());
	}
	
	
	public void new_BODEGA_J_accede_a_la_partie_recherche_globale_en_cliquant_sur_l_onglet_Projets() throws InterruptedException {
		
		Thread.sleep(1000);
		Actions action = new Actions(driver);
		WebElement we = driver.findElement(By.xpath("//*[@id=\"main_menu\"]/ul/li[3]/a"));
		action.moveToElement(we).build().perform();
		Thread.sleep(1000);
		action.moveToElement(driver.findElement(By.xpath("//*[@id=\"main_menu\"]/ul/li[3]/div/ul[4]/li[2]/a"))).click().build().perform();
	}
	
	
	public void new_BODEGA_Je_recherche_une_demande_a_partir_de_son_numero_d_identification_defini_dans_ADDOO_lors_de_sa_creation() throws InterruptedException {
		
		/* se connecter au site gassi */
		WebDriverWait wait = new WebDriverWait(driver, 10);
	
		System.out.println("coucou " + driver.findElements(By.xpath("//iframe")).size() +  " et ");
		
		
		final List<WebElement> iframes = driver.findElements(By.xpath("//iframe"));
	    for (WebElement iframe : iframes) {
	        System.out.println("cccccccccccc " + iframe.getAttribute("id") + "  " + iframe.getAttribute("name"));
	    }
	    
	    driver.switchTo().defaultContent();
	    driver.switchTo().frame(0);
		//WebElement iframe = driver.findElement(By.id("iframe_body"));
		//driver.switchTo().frame("iFrameName");
		
	    
		//Now find the element 
		WebElement search_input = wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("keyword")));
		search_input.clear();
		Thread.sleep(1000);
		search_input.sendKeys("00034639H71-18");
		
		
		//Once all your stuff done with this frame need to switch back to default
		//driver.switchTo().defaultContent();
		
	}
	
	
	public void new_BODEGA_Je_clique_sur_le_bouton_rechercher() throws InterruptedException {
		
		Thread.sleep(1000);
		driver.findElement(By.id("gs_search")).click();
	}
	
	
	
	public void new_BODEGA_Je_clique_sur_la_ligne_correspondante_a_notre_amenagement_de_site() {

		
		WebDriverWait wait = new WebDriverWait(driver, 15);
		
	/*	new WebDriverWait(driver, 20).until(ExpectedConditions
				.elementToBeClickable(By.xpath("//*[@id=\"global_search_res_container\"]/div/table[1]/tbody")));*/
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='overlay'][contains(@style, 'display: none')]")));
		
		List<WebElement> tbodydyElements = driver.findElements(By.xpath("//*[@id=\"global_search_res_container\"]/div/table[1]/tbody/tr"));
		

		if (tbodydyElements.size() > 2)
			tbodydyElements.get(1).click();
		
	}
	
}
