package seleniumPages;

import java.awt.AWTException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import common.Page_BasePage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class Page_AmenagementSiteNORIA extends Page_BasePage {

	public static final String NORIA_URL = "http://dvltewsc06-adm.rouen.francetelecom.fr/";
	public static final String GASSI_URL = "https://gassi.si.francetelecom.fr/";
	private String parentWindowHandler;
	 Actions builder;
	
	public Page_AmenagementSiteNORIA() {
		System.setProperty("webdriver.chrome.driver","C:\\\\Users\\\\A738696\\\\Desktop\\\\automated tests\\\\chromedriver_win32\\\\chromedriver.exe");
		parentWindowHandler = "";
	}
	

	public void launchBrowser() {
		driver = new ChromeDriver();
	//	driver.manage().window().maximize();
		System.out.println("Browser launched");
	}
	
	
	public void openNoriaInterface() {
		driver.get(GASSI_URL);
		driver.manage().window().maximize();
		System.out.println("[ADOO]User connected to the following URL: " + GASSI_URL);
		
		
		/* se connecter au site gassi */
		WebDriverWait wait = new WebDriverWait(driver, 10);
		//Find frame and switch
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("bgmainframe"));

		//Now find the element 
		WebElement user_input = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("user")));
		user_input.sendKeys("PGQG1130");
		
		WebElement password_input = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("password")));
		password_input.sendKeys("pgqg1130");
		
		//cliquer sur le bouton sign in afin de valider la saisie
		WebElement element = driver.findElement(By.id("spanLinkValidForm"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);

	
		//*[@id="LTE-04DEVnoria"]/a
		//*[@id="LTE-04DEVnoria"]
		//*[@id="tabapplis"]/tbody/tr[2]
		//*[@id="tabapplis"]/tbody/tr[2]/td[1] 
	//	System.out.println("dsdddddddddddd " + driver.findElements(By.xpath("//*[@id=\"tabapplis\"]/tbody/tr")).size());
	//	List<WebElement> rows = driver.findElements(By.xpath("//*[@id=\"tabapplis\"]/tbody/tr")).size();
		new WebDriverWait(driver, 20).until(ExpectedConditions
		.presenceOfElementLocated(By.xpath("//*[@id=\"tabapplis\"]/tbody/tr[4]/td[2]/a"))).click();
		
		
		//Once all your stuff done with this frame need to switch back to default
		driver.switchTo().defaultContent();
		
		//switch de vue( nouvelle window, noria ici)
		// Switch to new window opened
		
		// Switch to new window opened
		for(String winHandle : driver.getWindowHandles()){
			
		    driver.switchTo().window(winHandle);
		    System.out.println("abcdef " +  driver.getTitle());
		    if (driver.getTitle().equals("NORIA"))
		    	break;
		}
		
		
		System.out.println("okkkkkkkkkkkk " +  driver.getTitle());
	}
	
	
	public void noria_J_accede_a_la_partie_deploiement_de_NORIA_en_cliquant_sur_l_onglet_deploiement() throws InterruptedException {
	
		Thread.sleep(1000);
		 new WebDriverWait(driver, 20).until(ExpectedConditions
					.presenceOfElementLocated(By.xpath("//a[@href='/noria/nor/listeop/index/reset/1']"))).click();		
	}
	
	
	public void noria_Je_clique_sur_le_bouton_rechercher() throws InterruptedException {
		
		builder = new Actions(driver);
		Thread.sleep(2000);
		
		//recherche de type
		driver.findElement(By.xpath("//*[@id=\"typeSearchSelectContainer\"]/button")).click();
		Thread.sleep(1000);
	/*	builder.sendKeys(Keys.DOWN).build().perform();//press down arrow key
		Thread.sleep(1000);
		builder.sendKeys(Keys.ENTER).build().perform();*/
		
		//afficher le block de select  
		((JavascriptExecutor) driver).executeScript("document.getElementById('SearchTypeSelect').style.display='block';");
		//cacher l'input ainsi que son bouton correspondant
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebElement inputElement = driver.findElement(By.xpath("//*[@id=\"typeSearchSelectContainer\"]/input"));
		js.executeScript("arguments[0].setAttribute('style', 'display:none')",inputElement);
		
		WebElement buttoneElement = driver.findElement(By.xpath("//*[@id=\"typeSearchSelectContainer\"]/button"));
		js.executeScript("arguments[0].setAttribute('style', 'display:none')",buttoneElement);
		
		
		Select select = new Select(driver.findElement(By.id("SearchTypeSelect")));
	//	select.deselectAll();
		select.selectByValue("addoo");
		
	}
	
	public void noria_Je_selectionne_la_demande_et_je_clique_sur_initialiser() throws InterruptedException {
		
		//cliquer sur le bouton sign in afin de valider la saisie
	
	/*	WebElement element = driver.findElement(By.xpath("//*[@id=\"createWithoutAddoo\"]"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);*/
		 new WebDriverWait(driver, 5).until(ExpectedConditions
					.presenceOfElementLocated(By.xpath("//a[@href='/noria/nor/initop']"))).click();		
	
		
	}
	
	
	
	public void noria_Je_selectionne_le_code_PRG() throws InterruptedException {
		//recherche de type
		builder = new Actions(driver);
		
		//typage mkt1
		driver.findElement(By.xpath("//*[@id=\"groupeDeNoeudsProcess_datas\"]/table/tbody/tr[1]/td[6]/button")).click();	
		Thread.sleep(700);
		for(int i = 0; i < 5; i++){
			Thread.sleep(150);
			builder.sendKeys(Keys.DOWN).build().perform();//press down arrow key
		}
		Thread.sleep(1000);
		builder.sendKeys(Keys.ENTER).build().perform();//press down arrow key
		driver.findElement(By.xpath("//*[@id=\"donneesSiteFieldset\"]/div[2]/div/table/tbody/tr[1]/td[5]/b")).click();	
		Thread.sleep(1000);
		builder.keyDown(Keys.CONTROL).sendKeys(Keys.END).build().perform();
	
	//	driver.findElement(By.xpath("//*[@id=\"groupeDeNoeudsProcess_datas\"]/table/tbody/tr[1]/td[6]/button")).click();	
		
	}
	
	
	public void noria_Je_clique_sur_le_bouton_creer_sur_le_site_theorique() throws InterruptedException {

		Thread.sleep(1500);
		WebElement element = driver.findElement(By.id("buttonCreerSiteTheorique"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		
		
	}
	
	
	public void noria_Je_remplis_le_champ_adresse() throws InterruptedException {
		
		//recherche de type
		builder = new Actions(driver);
		Thread.sleep(600);
		driver.switchTo().activeElement();
	
		Thread.sleep(1000);
		driver.findElement(By.name("rechercheAdresse")).sendKeys("");
		driver.findElement(By.name("rechercheAdresse")).sendKeys("angers angers");
		Thread.sleep(1300);
		builder.sendKeys(Keys.DOWN).build().perform();//press down arrow key
		Thread.sleep(750);
		builder.sendKeys(Keys.ENTER).build().perform();//press enter arrow key
		
		driver.findElement(By.name("initializeAddress")).click();
		Thread.sleep(1000);
		driver.findElement(By.name("nomSite")).sendKeys("AMENAGEMENT_SCENARIO1");
		Thread.sleep(1000);
		builder.keyDown(Keys.CONTROL).sendKeys(Keys.END).build().perform();	
		System.out.println("bbbbbbbbbbbbbbbbb    " + driver.getTitle());
		Thread.sleep(1000);
	}
	
	
	public void noria_Je_clique_sur_le_bouton_sauvegarder() throws InterruptedException {
		
		//Find element by link text and store in variable "Element"        		
        WebElement element = driver.findElement(By.id("validerSite"));
        JavascriptExecutor js = (JavascriptExecutor) driver;

        //This will scroll the page till the element is found		
         js.executeScript("arguments[0].scrollIntoView();", element);
        Thread.sleep(1000);
        js.executeScript("arguments[0].click();", element);
		
        
        System.out.println("aaaaaaaaaaaaaaaa    " + driver.getTitle());
		driver.switchTo().activeElement();
		
		Thread.sleep(1500);
		//verifier si un site existe déjà
		if (driver.findElements( By.id("selectionnerSiteProximite") ).size() != 0) {
			System.out.println("je ne passe pas à l'interieur");
			Thread.sleep(1000);
			driver.findElement(By.id("selectionnerSiteProximite")).click();
		}
		
		Thread.sleep(1000);
	}
	
	
	public void noria_Je_clique_sur_le_bouton_modifier_le_site_theorique() throws InterruptedException {
	
		Thread.sleep(1500);
		driver.switchTo().activeElement();
		driver.findElement(By.id("buttonModifierSiteTheorique")).click();	
		Thread.sleep(1000);
		
		
		
		
	
	}
	
	
	public void noria_Je_modifie_la_valeur_en_Z_du_site_theorique() throws InterruptedException {
		
		Thread.sleep(1500);
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("document.getElementsByName('Z')[0].value='12';");
		Thread.sleep(1000);
	}
	
	
	public void noria_Je_clique_sur_le_bouton_sauvegarder_afin_de_valider_la_modification_de_la_valeur_en_Z() throws InterruptedException {
		//Find element by link text and store in variable "Element"        		
        WebElement element = driver.findElement(By.id("sauvegarderSite"));
        JavascriptExecutor js = (JavascriptExecutor) driver;

        //This will scroll the page till the element is found		
         js.executeScript("arguments[0].scrollIntoView();", element);
        Thread.sleep(1000);
        js.executeScript("arguments[0].click();", element);
	}
	
	
	public void noria_J_accede_a_la_page_permettant_le_traitement_d_une_operation_en_cliquant_sur_le_bouton_valider() throws InterruptedException {
		
		driver.switchTo().activeElement();
		Thread.sleep(1000);
		driver.findElement(By.id("valider")).click();
		
	}
	
	
}