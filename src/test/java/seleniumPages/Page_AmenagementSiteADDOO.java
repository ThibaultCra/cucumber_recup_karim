package seleniumPages;

import java.awt.AWTException;
import java.io.Console;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.w3c.dom.xpath.XPathResult;

import common.Page_BasePage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class Page_AmenagementSiteADDOO extends Page_BasePage {

	 public static final String ADDOO_URL = "http://dvltewsc06-adm.rouen.francetelecom.fr/";
	 Actions builder;
	
	public Page_AmenagementSiteADDOO() {
		System.setProperty("webdriver.chrome.driver","C:\\Users\\A738696\\Desktop\\orangeTechnique\\automated tests\\chromedriver_win32\\chromedriver.exe");
	}
	
	public void launchBrowser() {
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		System.out.println("Browser launched");
	}
	
	public void openAdooInterface() {
		driver.get(ADDOO_URL);
		System.out.println("[ADOO]User connected to the following URL: " + ADDOO_URL);
	}
	
	
	public void je_remplis_l_UPR_avec_UPR_Sud_Ouest_et_l_utilisateur() throws InterruptedException {
		builder = new Actions(driver);
		Thread.sleep(2000);
		//s�lectionner l'UPR
		driver.findElement(By.name("UR")).click();
		Thread.sleep(500);
		
		//pourrait faire un do while si on ne connait pas la taille du combobox
		for(int i = 0; i <= 4; i++){
			Thread.sleep(350);
		    builder.sendKeys(Keys.DOWN).build().perform();//press down arrow key
		}
		
		Thread.sleep(350);
		builder.sendKeys(Keys.ENTER).build().perform();//press enter
		
	//	Select uprValue = new Select(driver.findElement(By.xpath("//select[@name='UR']")));
	//	uprValue.selectByVisibleText("UPR Sud-Ouest");	
	//	System.out.println("osqdsqsdk  " + driver.findElement(By.xpath("//select[@name='UR']/option[@value='1']")).getText());
		
		
		//s�lectionner l'utilisateur
		driver.findElement(By.name("LISTE_N")).click();
		Thread.sleep(500);
		
		//pourrait faire un do while si on ne connait pas la taille du combobox
		for(int i = 0; i <= 5; i++){
			Thread.sleep(310);
		    builder.sendKeys(Keys.DOWN).build().perform();//press down arrow key
		}
		
		Thread.sleep(350);
		builder.sendKeys(Keys.ENTER).build().perform();//press enter
			
	}
	
	
	public void je_clique_sur_le_bouton_Acceder_a_ADDOO() {
		
		//cliquer sur le bouton 'acc�der � addoo' afin de valider la saisie
		WebElement element = driver.findElement(By.xpath("/html/body/center/form/table/tbody/tr[4]/td[2]/input[2]"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
	}
	
	
	public void je_me_trouve_dans_le_sous_onglet_Details_demande_de_l_onglet_demande_sur_l_interface_ADDOO() throws InterruptedException {
		
		Thread.sleep(1000);
		//cliquer sur le bouton 'acc�der au sous-onglet d�tails demande de l'onglet Demandes
		WebElement element = driver.findElement(By.xpath("/html/body/table/tbody/tr[2]/td[1]/font/table[8]/tbody/tr/td/a"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);
		
		
	}
	
	
	public void je_renseigne_le_nom_de_la_demande_et_le_DR() throws InterruptedException {
		
		builder = new Actions(driver);
		Thread.sleep(1000);
		WebElement user_input = driver.findElement(By.name("SITE_NOM_DEMANDE"));
		user_input.sendKeys("Aménagement-Scénario1");
		
		Thread.sleep(1000);
		//s�lectionner l'utilisateur
		driver.findElement(By.name("SITE_DR")).click();
		for(int i = 0; i <= 9; i++){
			Thread.sleep(310);
		    builder.sendKeys(Keys.UP).build().perform();//press down arrow key
		}
		
		Thread.sleep(350);
		builder.sendKeys(Keys.ENTER).build().perform();//press enter	
	}
	
	
	
	public void je_remplis_les_champs_necessaires_dans_la_partie_Operation() throws InterruptedException {
	    
		builder = new Actions(driver);
		// Write code here that turns the phrase above into concrete actions
		Thread.sleep(350);
		WebElement operation_panel =  driver.findElement(By.xpath("/html/body/table/tbody/tr[2]/td[2]/form/div[1]/input[1]"));
		
		//seulement l'ouvrir si il est ferm�
		if (operation_panel.getAttribute("value").equals("+")) {
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", operation_panel);
		}
		
		
		Thread.sleep(1000);
		
		//s�lectionner l'utilisateur
		driver.findElement(By.name("SITE_NUM_TYPE_OP")).click();
		
		builder.keyDown(Keys.CONTROL).sendKeys(Keys.HOME).build().perform();
		Thread.sleep(700);
		for(int i = 0; i <= 13; i++){
			Thread.sleep(250);
		    builder.sendKeys(Keys.DOWN).build().perform();//press down arrow key
		}
		
	  
	}

	public void je_remplis_les_champs_necessaires_dans_la_partie_Localisation_MKT() throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
	 
		builder = new Actions(driver);
		Thread.sleep(350);
		WebElement localisation_mkt_panel =  driver.findElement(By.xpath("/html/body/table/tbody/tr[2]/td[2]/form/div[3]/input[1]"));
		
		//seulement l'ouvrir si il est ferm�
		if (localisation_mkt_panel.getAttribute("value").equals("+")) {
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", localisation_mkt_panel);
		}
		
		WebElement code_contour_mkt = driver.findElement(By.name("SITE_CODE_CONTOUR_MKT"));
		code_contour_mkt.sendKeys("013Z1014");
		
		WebElement x_theorique = driver.findElement(By.name("SITE_X"));
		x_theorique.sendKeys("93250");
		
		WebElement y_theorique = driver.findElement(By.name("SITE_Y"));
		y_theorique.sendKeys("1124856");
		
		
		//morphologie
		driver.findElement(By.name("SITE_MORPHOLOGIE")).click();
		Thread.sleep(700);
		for(int i = 0; i <= 1; i++){
			Thread.sleep(250);
		    builder.sendKeys(Keys.UP).build().perform();//press down arrow key
		}
		Thread.sleep(1100);
		driver.findElement(By.name("SITE_MORPHOLOGIE")).click();
		Thread.sleep(1500);
		
		//phase
		driver.findElement(By.name("SITE_PHASE")).click();	
		builder.keyDown(Keys.CONTROL).sendKeys(Keys.HOME).build().perform();
		Thread.sleep(700);
		for(int i = 0; i <= 5; i++){
			Thread.sleep(280);
			builder.sendKeys(Keys.DOWN).build().perform();//press down arrow key
		}
		Thread.sleep(1100);
		driver.findElement(By.name("SITE_PHASE")).click();	
		Thread.sleep(1500);
		
		
		//bloc
		driver.findElement(By.name("SITE_BLOC")).click();	
		builder.keyDown(Keys.CONTROL).sendKeys(Keys.HOME).build().perform();
		Thread.sleep(1100);
		driver.findElement(By.name("SITE_BLOC")).click();	
		Thread.sleep(1500);
		
		
		//choisir la zone de vie
		driver.findElement(By.xpath("//*[@id=\"mkt\"]/table/tbody/tr[6]/td[2]/input[2]")).click();
		Thread.sleep(700);
		
		String parentWindowHandler = driver.getWindowHandle(); // Store your parent window
		String subWindowHandler = null;

		Set<String> handles = driver.getWindowHandles(); // get all window handles
		Iterator<String> iterator = handles.iterator();
		while (iterator.hasNext()){
		    subWindowHandler = iterator.next();
		}
		driver.switchTo().window(subWindowHandler); // switch to popup window

		// Now you are in the popup window, perform necessary actions here
		WebElement filter_zone_de_vie = driver.findElement(By.xpath("/html/body/center/form/input[1]"));
		filter_zone_de_vie.sendKeys("angers");
		Thread.sleep(1000);
		
		WebElement okButton_zone_de_vie = driver.findElement(By.xpath("/html/body/center/form/input[2]"));
		JavascriptExecutor executor_zone_de_vie_button = (JavascriptExecutor)driver;
		executor_zone_de_vie_button.executeScript("arguments[0].click();", okButton_zone_de_vie);
		Thread.sleep(1000);
		
		driver.switchTo().window(parentWindowHandler);  // switch back to parent window
		
	
		//SITE_Z_MKT
		driver.findElement(By.name("SITE_Z_MKT")).click();	
		Thread.sleep(700);
		builder.sendKeys(Keys.UP).build().perform();//press down arrow key
		Thread.sleep(1000);
		driver.findElement(By.name("SITE_Z_MKT")).click();	
		Thread.sleep(1500);
		
		
	}

	
	public void je_remplis_les_champs_necessaires_dans_la_partie_Details_GSM() throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
		builder = new Actions(driver);
		Thread.sleep(700);
		
		WebElement details_gsm_panel =  driver.findElement(By.xpath("/html/body/table/tbody/tr[2]/td[2]/form/div[5]/input[1]"));
		//seulement l'ouvrir si il est ferm�
		if (details_gsm_panel.getAttribute("value").equals("+")) {
			JavascriptExecutor executor = (JavascriptExecutor)driver;
			executor.executeScript("arguments[0].click();", details_gsm_panel);
		}
		
		
		//scroll element into center of view
		Point p = details_gsm_panel.getLocation();
		((JavascriptExecutor) driver).executeScript("window.scroll(" + p.getX() + "," + (p.getY() - 200) + ");");
		Thread.sleep(700);
		
		
		//GSM_OPERATION
		driver.findElement(By.name("GSM_OPERATION")).click();	
		Thread.sleep(700);
		builder.sendKeys(Keys.DOWN).build().perform();//press down arrow key
		Thread.sleep(1000);
		driver.findElement(By.name("GSM_OPERATION")).click();
		Thread.sleep(1500);
	
		//bandes gsm
		driver.findElement(By.name("GSM_BANDES")).click();	
		builder.keyDown(Keys.CONTROL).sendKeys(Keys.HOME).build().perform();
		Thread.sleep(700);
		for(int i = 0; i < 3; i++){
			Thread.sleep(280);
			builder.sendKeys(Keys.UP).build().perform();//press down arrow key
		}
		Thread.sleep(1000);
		driver.findElement(By.name("GSM_BANDES")).click();	
		Thread.sleep(1500);
		
		
		//typage mkt1
		driver.findElement(By.name("GSM_TYPAGE_MKT1")).click();	
		builder.keyDown(Keys.CONTROL).sendKeys(Keys.HOME).build().perform();
		Thread.sleep(700);
		for(int i = 0; i < 4; i++){
			Thread.sleep(280);
			builder.sendKeys(Keys.DOWN).build().perform();//press down arrow key
		}
		Thread.sleep(1000);
		driver.findElement(By.name("GSM_TYPAGE_MKT1")).click();	
		Thread.sleep(1500);
		
		
		//typage mkt1
		driver.findElement(By.name("GSM_TYPAGE_MKT2")).click();	
		Thread.sleep(700);
		for(int i = 0; i < 2; i++){
			Thread.sleep(280);
			builder.sendKeys(Keys.DOWN).build().perform();//press down arrow key
		}
		Thread.sleep(1000);
		driver.findElement(By.name("GSM_TYPAGE_MKT2")).click();	
		Thread.sleep(1500);
	
	
		//priorit�
		driver.findElement(By.name("GSM_PRIORITE_DEC")).click();	
		builder.keyDown(Keys.CONTROL).sendKeys(Keys.HOME).build().perform();
		Thread.sleep(1000);
		driver.findElement(By.name("GSM_PRIORITE_DEC")).click();	
		Thread.sleep(1500);
		
		
		//priorit�
		driver.findElement(By.name("GSM_PROCESS_ORG")).click();	
		Thread.sleep(700);
		builder.sendKeys(Keys.UP).build().perform();//press down arrow key
		Thread.sleep(1000);
		driver.findElement(By.name("GSM_PROCESS_ORG")).click();	
		Thread.sleep(1500);
		
		//gsm objectif
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("document.getElementsByName('GSM_OBJECTIF')[0].value='testscenario1';");
	}
	
	
	public void je_clique_sur_le_bouton_Ajouter() {
	    // Write code here that turns the phrase above into concrete actions
	  
	}

	
	public void le_numero_de_la_demande_apparait() {
	    // Write code here that turns the phrase above into concrete actions

	}

	
	public void des_champs_de_la_partie_localisation_MKT_apparaissent_avec_un_fond_bleu_clair() {
	    // Write code here that turns the phrase above into concrete actions

	}


	public void checkSearchBoxIsDisplayed() throws InterruptedException {
		
		System.out.println("fdsdf");
		
		WebDriverWait wait = new WebDriverWait(driver, 10);

		//Find frame and switch
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("bgmainframe"));

		//Now find the element 
		WebElement user_input = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("user")));
		user_input.sendKeys("PGQG1130");
		
		WebElement password_input = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("password")));
		password_input.sendKeys("pgqg1130");
		
		//cliquer sur le bouton sign in afin de valider la saisie
		WebElement element = driver.findElement(By.id("spanBtnCnx"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);

		//Once all your stuff done with this frame need to switch back to default
		driver.switchTo().defaultContent();
	
		
		//nous r�cup�rons les �l�ments de la seconde colonne de chaque ligne
		List<WebElement> elements = driver.findElements(By.xpath(".//*/tr/td[2]/a"));
		for(WebElement elem : elements){
			//acc�der � la liste des applications. Dans ce test nous allons s�lectionner NewBodega
			if (elem.getText().equals("NEWBODEGA")) {
				System.out.println("found NewBodega");
				/** Be aware that ther eis 2 new bodega lines, we have to sort it according to the need 
				 * (admin in our case)
				 */
				//click on href link to access to the new bodega interface
				elem.click();
			}
		}
		
		
		//set up nested functions
		
		
		//driver.findElement(By.name("username")).sendKeys("okkk");
		
		/*WebElement addTrainer = (new WebDriverWait(driver, 10)).until(ExpectedConditions.elementToBeClickable(By.name("linkHelp")));
		addTrainer.click();*/
		
	/*	WebDriverWait wait = new WebDriverWait(driver, 8);
		WebElement Category_Body = wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("USER")));
		 Category_Body.sendKeys("87654321");*/
		 
		
		//System.out.print(driver.findElement(By.name("USER")));  spanLinkValidForm
		  
/*		if(driver.findElement(By.name("USER")).isDisplayed()) {
			System.out.println("Search text box is displayed");
		} else {
			System.out.println("Search text box is NOT displayed");
		}*/
	}

	public void checkGoogleSearchButtonIsDisplayed() {
	/*	driver.findElement(By.name("q")).sendKeys("test orange");
		
		WebElement element = driver.findElement(By.name("btnK"));
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].click();", element);*/
		
		/*if(driver.findElement(By.name("btnK")).isDisplayed()) {
			System.out.println("Google Search button is displayed");
		} else {
			System.out.println("Google Search button is NOT displayed");
		}*/
	}
/** [ADDOO]
* 	WIP
* Valider l'UPR de la demande d'amenagement creee 
*/
	
	public void une_demande_d_amenagement_a_ete_creee_avec_un_UPR_specifique() {
	    //TODO
	}
	public void j_ai_fini_de_verifier_l_UPR_rentree_par_l_utilisateur() {
	    //TODO
	}
	public void je_clique_sur_le_bouton_Valider_UPR() {
	    //TODO
	}
	public void la_checkbox_Valider_UPR_de_la_partie_operation_a_ete_cochee() {
		//TODO
	}
	
/** [ADDOO]
* 	WIP
* Valider le siege de la demande d'amenagement creee 
*/
	
	public void une_demande_d_amenagement_a_ete_creee_et_l_utilisateur_vient_de_valider_l_UPR() {
	    //TODO
	}
	public void j_ai_fini_de_verifier_le_siege_rentree_par_l_utilisateur() {
	    //TODO
	}
	public void addoo_Je_clique_sur_le_bouton_Valider_siege() {
	    //TODO
	}
	public void addoo_La_checkbox_Valider_siege_de_la_partie_operation_a_ete_cochee() {
		//TODO
	}
	
/** [ADDOO]
* 	WIP
* Fonctions du scénario demande en erreur  
*/
	
	public void addoo_Une_demande_de_la_part_de_addoo_a_ete_rejetee_par_noria() {	
		//TODO
	}
	public void un_message_d_erreur_est_indique_dans_l_onglet_demandes_en_erreur() {
		//TODO
	}
	public void je_change_les_valeurs_correspondantes_sur_l_interface_de_demande_pour_traiter_l_erreur() {
		//TODO
	}
	public void j_enregistre_les_nouvelles_valeurs_en_cliquant_sur_enregistrer() {
		//TODO
	}
	public void j_annule_la_validation_de_siege() {
		//TODO
	}
	public void j_annule_la_validation_d_upr() {
		//TODO
	}
	public void je_valide_de_nouveau_l_upr() {
		//TODO
	}
	public void je_valide_de_nouveau_le_siege() {
		//TODO
	}
	public void la_demande_ne_doit_plus_etre_en_erreur() {
		//TODO
	}

}
