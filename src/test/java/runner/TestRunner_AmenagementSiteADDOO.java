package runner;

import java.io.File;

import org.junit.AfterClass;
import org.junit.runner.RunWith;

import com.cucumber.listener.Reporter;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/java/features/AmenagementSiteADDOO.feature", glue = "stepDefinitions", 
				 plugin = { "com.cucumber.listener.ExtentCucumberFormatter:target/cucumber-reports/report.html" },
				 monochrome = true)
/*
 * @CucumberOptions(features=
 * "src/test/java/features/AmenagementSiteADDOO.feature",
 * glue="stepDefinitions", plugin= {"pretty", "html:target/htmlreports"},
 * monochrome = true)
 */

public class TestRunner_AmenagementSiteADDOO {

	@AfterClass
	public static void writeExtentReport() {
		Reporter.loadXMLConfig(new File("config/report.xml"));

	}

}