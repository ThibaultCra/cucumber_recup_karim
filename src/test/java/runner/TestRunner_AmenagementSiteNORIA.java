package runner;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
 
@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/java/features/AmenagementSiteNORIA.feature", glue="stepDefinitions",
				 plugin = {"pretty"}, monochrome = true)
public class TestRunner_AmenagementSiteNORIA {
 
}