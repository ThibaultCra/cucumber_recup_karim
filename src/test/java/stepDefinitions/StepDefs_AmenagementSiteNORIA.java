package stepDefinitions;


import java.awt.AWTException;


import cucumber.api.java.en.*;
import seleniumPages.Page_AmenagementSiteNORIA;

public class StepDefs_AmenagementSiteNORIA  {
	
	Page_AmenagementSiteNORIA noriaPage = new Page_AmenagementSiteNORIA();


@Given("^\\[NORIA\\]Je suis sur la page d'accueil$")
	public void noria_Je_suis_sur_la_page_d_accueil() {
	    // Write code here that turns the phrase above into concrete actions
		noriaPage.launchBrowser();
		noriaPage.openNoriaInterface();
	}

	@When("^\\[NORIA\\]J'accede a la partie deploiement de NORIA en cliquant sur l'onglet deploiement$")
	public void noria_J_accede_a_la_partie_deploiement_de_NORIA_en_cliquant_sur_l_onglet_deploiement() throws InterruptedException {
	    noriaPage.noria_J_accede_a_la_partie_deploiement_de_NORIA_en_cliquant_sur_l_onglet_deploiement();
	}

	@When("^\\[NORIA\\]Je recherche une demande a partir de son numero d'identification defini dans ADDOO lors de sa creation$")
	public void noria_Je_recherche_une_demande_a_partir_de_son_numero_d_identification_defini_dans_ADDOO_lors_de_sa_creation() {
	    // Write code here that turns the phrase above into concrete actions
	}

	@When("^\\[NORIA\\]Je clique sur le bouton rechercher$")
	public void noria_Je_clique_sur_le_bouton_rechercher() throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
		noriaPage.noria_Je_clique_sur_le_bouton_rechercher();
	}

	@When("^\\[NORIA\\]Je selectionne la demande et je clique sur initialiser$")
	public void noria_Je_selectionne_la_demande_et_je_clique_sur_initialiser() throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
		noriaPage.noria_Je_selectionne_la_demande_et_je_clique_sur_initialiser();
	}

	@Then("^\\[NORIA\\]L'utilisateur accede a une nouvelle page Initialisation d'operation$")
	public void noria_L_utilisateur_accede_a_une_nouvelle_page_Initialisation_d_operation() {
	    // Write code here that turns the phrase above into concrete actions
	}

	@Given("^\\[NORIA\\]Je me trouve sur la page d'Initialisation d'operation$")
	public void noria_Je_me_trouve_sur_la_page_d_Initialisation_d_operation() {
	    // Write code here that turns the phrase above into concrete actions
	}

	@When("^\\[NORIA\\]Je selectionne le code PRG$")
	public void noria_Je_selectionne_le_code_PRG() throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
		noriaPage.noria_Je_selectionne_le_code_PRG();
	}

	@When("^\\[NORIA\\]Je clique sur le bouton creer sur le site theorique$")
	public void noria_Je_clique_sur_le_bouton_creer_sur_le_site_theorique() throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
		noriaPage.noria_Je_clique_sur_le_bouton_creer_sur_le_site_theorique();
	}

	@When("^\\[NORIA\\]Je remplis le champ adresse$")
	public void noria_Je_remplis_le_champ_adresse() throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
		noriaPage.noria_Je_remplis_le_champ_adresse();
	}

	@When("^\\[NORIA\\]Je clique sur le bouton sauvegarder$")
	public void noria_Je_clique_sur_le_bouton_sauvegarder() throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
		noriaPage.noria_Je_clique_sur_le_bouton_sauvegarder();
	}

	@Then("^\\[NORIA\\] Un site theorique vient d'etre cree et les informations relatives au site sont mises a jour dans la page principale de l'Initialisation d'operation$")
	public void noria_Un_site_theorique_vient_d_etre_cree_et_les_informations_relatives_au_site_sont_mises_a_jour_dans_la_page_principale_de_l_Initialisation_d_operation() {
	    // Write code here that turns the phrase above into concrete actions
	}

	@Given("^\\[NORIA\\]Je me trouve sur la page d'Initialisation d'operation et l'utilisateur vient de creer un site theorique$")
	public void noria_Je_me_trouve_sur_la_page_d_Initialisation_d_operation_et_l_utilisateur_vient_de_creer_un_site_theorique() {
	    // Write code here that turns the phrase above into concrete actions
	}

	@When("^\\[NORIA\\]Je clique sur le bouton modifier le site theorique$")
	public void noria_Je_clique_sur_le_bouton_modifier_le_site_theorique() throws InterruptedException {
	    
		noriaPage.noria_Je_clique_sur_le_bouton_modifier_le_site_theorique();
	}

	@When("^\\[NORIA\\]Je modifie la valeur en Z du site theorique$")
	public void noria_Je_modifie_la_valeur_en_Z_du_site_theorique() throws InterruptedException {
	  noriaPage.noria_Je_modifie_la_valeur_en_Z_du_site_theorique();
	}
	
	@When("^\\[NORIA\\]Je clique sur le bouton sauvegarder afin de valider la modification de la valeur en Z$")
	public void noria_Je_clique_sur_le_bouton_sauvegarder_afin_de_valider_la_modification_de_la_valeur_en_Z() throws InterruptedException {
	    noriaPage.noria_Je_clique_sur_le_bouton_sauvegarder_afin_de_valider_la_modification_de_la_valeur_en_Z();
	}

	@Then("^\\[NORIA\\]La valeur en Z du site theorique a ete modifiee et sa modification est visible sur la page d'Initialisation d'operation$")
	public void noria_La_valeur_en_Z_du_site_theorique_a_ete_modifiee_et_sa_modification_est_visible_sur_la_page_d_Initialisation_d_operation() {
	    // Write code here that turns the phrase above into concrete actions
	}

	@Given("^\\[NORIA\\]Je viens de creer et modifier un site theorique$")
	public void addoo_Je_viens_de_creer_et_modifier_un_site_theorique() {
	    // Write code here that turns the phrase above into concrete actions
	}

	
	@Given("^\\[NORIA\\]J'accede a la page permettant le traitement d'une operation en cliquant sur le bouton valider$")
	public void noria_J_accede_a_la_page_permettant_le_traitement_d_une_operation_en_cliquant_sur_le_bouton_valider() throws InterruptedException {
	    noriaPage.noria_J_accede_a_la_page_permettant_le_traitement_d_une_operation_en_cliquant_sur_le_bouton_valider();
	}


	@When("^\\[NORIA\\]Je clique sur le bouton details de l'operation$")
	public void addoo_Je_clique_sur_le_bouton_details_de_l_operation() {
	    // Write code here that turns the phrase above into concrete actions
	}

	@When("^\\[NORIA\\]Je modifie le rayon de recherche$")
	public void addoo_Je_modifie_le_rayon_de_recherche() {
	    // Write code here that turns the phrase above into concrete actions
	}

	@When("^\\[NORIA\\]Je valide ma saisie en cliquant sur le bouton sauvegarder$")
	public void addoo_Je_valide_ma_saisie_en_cliquant_sur_le_bouton_sauvegarder() {
	    // Write code here that turns the phrase above into concrete actions
	}

	@Then("^\\[NORIA\\]L'utilisateur revient sur la page traitement d'operation avec le rayon de recherche modifie$")
	public void addoo_L_utilisateur_revient_sur_la_page_traitement_d_operation_avec_le_rayon_de_recherche_modifie() {
	    // Write code here that turns the phrase above into concrete actions
	}

	@Given("^\\[NORIA\\]Je suis sur la page permettant de traiter une operation et je viens de modifier le rayon de recherche du site theorique$")
	public void noria_Je_suis_sur_la_page_permettant_de_traiter_une_operation_et_je_viens_de_modifier_le_rayon_de_recherche_du_site_theorique() {
	    // Write code here that turns the phrase above into concrete actions
	}

	@When("^\\[NORIA\\]Je clique sur le bouton ajouter au niveau des elements de reseau$")
	public void noria_Je_clique_sur_le_bouton_ajouter_au_niveau_des_elements_de_reseau() {
	    // Write code here that turns the phrase above into concrete actions
	}

	@When("^\\[NORIA\\]Je clique sur sauvegarder en ne modifiant aucune valeur$")
	public void noria_Je_clique_sur_sauvegarder_en_ne_modifiant_aucune_valeur() {
	    // Write code here that turns the phrase above into concrete actions
	}

	@Then("^\\[NORIA\\]Un element de reseau vient d'etre cree et celui-ci est visible avec un fond orange$")
	public void noria_Un_element_de_reseau_vient_d_etre_cree_et_celui_ci_est_visible_avec_un_fond_orange() {
	    // Write code here that turns the phrase above into concrete actions
	}

	@Given("^\\[NORIA\\]Je viens d'ajouter un element de reseau$")
	public void noria_Je_viens_d_ajouter_un_element_de_reseau() {
	    // Write code here that turns the phrase above into concrete actions
	}

	@When("^\\[NORIA\\]Je clique sur le bouton ajouter au niveau des noeuds cellulaires$")
	public void noria_Je_clique_sur_le_bouton_ajouter_au_niveau_des_noeuds_cellulaires() {
	    // Write code here that turns the phrase above into concrete actions
	}

	@When("^\\[NORIA\\]Je modifie les valeurs lies a la description du noeud cellulaire$")
	public void noria_Je_modifie_les_valeurs_lies_a_la_description_du_noeud_cellulaire() {
	    // Write code here that turns the phrase above into concrete actions
	}

	@When("^\\[NORIA\\]Je clique sur sauvegarder$")
	public void noria_Je_clique_sur_sauvegarder() {
	    // Write code here that turns the phrase above into concrete actions
	}

	@Then("^\\[NORIA\\]Un noeud cellulaire vient d'etre cree et celui-ci est visible avec un fond orange$")
	public void noria_Un_noeud_cellulaire_vient_d_etre_cree_et_celui_ci_est_visible_avec_un_fond_orange() {
	    // Write code here that turns the phrase above into concrete actions
	}

	@Given("^\\[NORIA\\]Je viens de creer un element de reseau et un noeud cellulaire$")
	public void noria_Je_viens_de_creer_un_element_de_reseau_et_un_noeud_cellulaire() {
	    // Write code here that turns the phrase above into concrete actions
	}

	
	@When("^\\[NORIA\\]Je clique sur le bouton valider FN(\\d+) dans la partie Actions sur le candidat$")
	public void noria_Je_clique_sur_le_bouton_valider_FN_dans_la_partie_Actions_sur_le_candidat(int arg1)  {
	    // Write code here that turns the phrase above into concrete actions

	}

	@When("^\\[NORIA\\]Je valide la date de validation$")
	public void noria_Je_valide_la_date_de_validation() {
	    // Write code here that turns the phrase above into concrete actions
	}

	@Then("^\\[NORIA\\]L'amenagement de l'operation a ete accepte et valide$")
	public void noria_L_amenagement_de_l_operation_a_ete_accepte_et_valide() {
	    // Write code here that turns the phrase above into concrete actions
	}


	@Given("^\\[NORIA\\]Je me trouve sur l'interface ADDOO et je viens de valider une fiche navette d'amenagement pour l'operation en cours$")
	public void noria_Je_me_trouve_sur_l_interface_ADDOO_et_je_viens_de_valider_une_fiche_navette_d_amenagement_pour_l_operation_en_cours() {
	    // Write code here that turns the phrase above into concrete actions
	}

	@When("^\\[NORIA\\]J'accede aux demandes en cliquant sur le sous-onglet Demandes integrees de l'onglet Suivi$")
	public void noria_J_accede_aux_demandes_en_cliquant_sur_le_sous_onglet_Demandes_integrees_de_l_onglet_Suivi() {
	    // Write code here that turns the phrase above into concrete actions
	}

	@Then("^\\[NORIA\\]La demande d'amenagement nommee AMENAGEMENT_SCENARIO_UN est visible dans la liste$")
	public void noria_La_demande_d_amenagement_nommee_AMENAGEMENT_SCENARIO_UN_est_visible_dans_la_liste() {
	    // Write code here that turns the phrase above into concrete actions
	}
	
}
