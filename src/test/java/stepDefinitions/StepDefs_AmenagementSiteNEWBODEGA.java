package stepDefinitions;


import java.awt.AWTException;

import cucumber.api.java.en.*;
import seleniumPages.Page_AmenagementSiteNEWBODEGA;

public class StepDefs_AmenagementSiteNEWBODEGA  {

	Page_AmenagementSiteNEWBODEGA newBodegaPage = new Page_AmenagementSiteNEWBODEGA();
	
	
	
	
	@Given("^\\[NEW BODEGA\\]Je suis sur la page d'accueil$")
	public void new_BODEGA_Je_suis_sur_la_page_d_accueil() {
	    // Write code here that turns the phrase above into concrete actions
		newBodegaPage.launchBrowser();
		newBodegaPage.openNewBodegaInterface();
	}
	

	@When("^\\[NEW BODEGA\\]J'accede a la partie recherche globale en cliquant sur l'onglet Projets$")
	public void new_BODEGA_J_accede_a_la_partie_recherche_globale_en_cliquant_sur_l_onglet_Projets() throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
		newBodegaPage.new_BODEGA_J_accede_a_la_partie_recherche_globale_en_cliquant_sur_l_onglet_Projets();
	}

	@When("^\\[NEW BODEGA\\]Je recherche une demande a partir de son numero d'identification defini dans ADDOO lors de sa creation$")
	public void new_BODEGA_Je_recherche_une_demande_a_partir_de_son_numero_d_identification_defini_dans_ADDOO_lors_de_sa_creation() throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
		newBodegaPage.new_BODEGA_Je_recherche_une_demande_a_partir_de_son_numero_d_identification_defini_dans_ADDOO_lors_de_sa_creation();
	}

	@When("^\\[NEW BODEGA\\]Je clique sur le bouton rechercher$")
	public void new_BODEGA_Je_clique_sur_le_bouton_rechercher() throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
		newBodegaPage.new_BODEGA_Je_clique_sur_le_bouton_rechercher();
	}

	@When("^\\[NEW BODEGA\\]Je clique sur la ligne correspondante a notre amenagement de site$")
	public void new_BODEGA_Je_clique_sur_la_ligne_correspondante_a_notre_amenagement_de_site() {
	    // Write code here that turns the phrase above into concrete actions
		newBodegaPage.new_BODEGA_Je_clique_sur_la_ligne_correspondante_a_notre_amenagement_de_site();
	}

	@Then("^\\[NEW BODEGA\\]L'utilisateur accede a une nouvelle page affichant les informations de l'amenagement$")
	public void new_BODEGA_L_utilisateur_accede_a_une_nouvelle_page_affichant_les_informations_de_l_amenagement() {
	    // Write code here that turns the phrase above into concrete actions
	}
}
