package stepDefinitions;


import java.awt.AWTException;

import cucumber.api.PendingException;
import cucumber.api.java.en.*;
import seleniumPages.Page_AmenagementSiteADDOO;

public class StepDefs_AmenagementSiteADDOO  {

	Page_AmenagementSiteADDOO addooPage = new Page_AmenagementSiteADDOO();
	
	
	@Given("^\\[ADDOO\\]Je suis sur la page d'authentification$")
	public void je_suis_sur_la_page_d_authentification() {
	    // Write code here that turns the phrase above into concrete actions
	    addooPage.launchBrowser();
	    addooPage.openAdooInterface();
	}
	

	
	@When("^\\[ADDOO\\]Je remplis l\\'UPR avec UPR Sud-Ouest et l\\'utilisateur$")
	public void addoo_Je_remplis_l_UPR_avec_UPR_Sud_Ouest_et_l_utilisateur()  throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
		addooPage.je_remplis_l_UPR_avec_UPR_Sud_Ouest_et_l_utilisateur();
	}

	
	@When("^\\[ADDOO\\]Je clique sur le bouton Acceder a ADDOO$")
	public void addoo_Je_clique_sur_le_bouton_Acceder_a_ADDOO(){
	    // Write code here that turns the phrase above into concrete actions
		addooPage.je_clique_sur_le_bouton_Acceder_a_ADDOO();
	}

	@Then("^\\[ADDOO\\]Je devrais acceder a l'interface ADDOO avec le bon UPR et utilisateur$")
	public void je_devrais_acceder_a_l_interface_ADDOO_avec_le_bon_UPR_et_utilisateur() {
	    // Write code here that turns the phrase above into concrete actions
		System.out.println("Vous venez d'accéder à l'interface");
	}

	@Given("^\\[ADDOO\\]Je me trouve dans le sous-onglet Details demande de l\\'onglet demande sur l\\'interface ADDOO$")
	public void je_me_trouve_dans_le_sous_onglet_Details_demande_de_l_onglet_demande_sur_l_interface_ADDOO() throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
		addooPage.je_me_trouve_dans_le_sous_onglet_Details_demande_de_l_onglet_demande_sur_l_interface_ADDOO();
	}

	@When("^\\[ADDOO\\]Je renseigne le nom de la demande et le DR$")
	public void je_renseigne_le_nom_de_la_demande_et_le_DR() throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
		addooPage.je_renseigne_le_nom_de_la_demande_et_le_DR();
	}

	@When("^\\[ADDOO\\]Je remplis les champs necessaires dans la partie Operation$")
	public void je_remplis_les_champs_necessaires_dans_la_partie_Operation() throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
		addooPage.je_remplis_les_champs_necessaires_dans_la_partie_Operation();
	}

	@When("^\\[ADDOO\\]Je remplis les champs necessaires dans la partie Localisation/MKT$")
	public void je_remplis_les_champs_necessaires_dans_la_partie_Localisation_MKT() throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
		addooPage.je_remplis_les_champs_necessaires_dans_la_partie_Localisation_MKT();
	}

	@When("^\\[ADDOO\\]Je remplis les champs necessaires dans la partie Details GSM$")
	public void je_remplis_les_champs_necessaires_dans_la_partie_Details_GSM() throws InterruptedException {
	    // Write code here that turns the phrase above into concrete actions
		addooPage.je_remplis_les_champs_necessaires_dans_la_partie_Details_GSM();
	
	}

	@When("^\\[ADDOO\\]je clique sur le bouton Ajouter$")
	public void je_clique_sur_le_bouton_Ajouter() {
	    // Write code here that turns the phrase above into concrete actions
	  
	}

	@Then("^\\[ADDOO\\]Le numero de la demande apparait$")
	public void le_numero_de_la_demande_apparait() {
	    // Write code here that turns the phrase above into concrete actions

	}

	@Then("^\\[ADDOO\\]Des champs de la partie localisation/MKT apparaissent avec un fond bleu clair$")
	public void des_champs_de_la_partie_localisation_MKT_apparaissent_avec_un_fond_bleu_clair() {
	    // Write code here that turns the phrase above into concrete actions

	}
	
/**Scenario: [ADDOO]
* WIP
* Valider l'UPR de la demande d'amenagement creee
*/
	
	@Given("^\\[ADDOO\\]Une demande d'amenagement a ete creee avec un UPR specifique$")
	public void une_demande_d_amenagement_a_ete_creee_avec_un_UPR_specifique() {
	    addooPage.une_demande_d_amenagement_a_ete_creee_avec_un_UPR_specifique();
	}
	@When("^\\[ADDOO\\]J'ai fini de verifier l'UPR rentree par l'utilisateur$")
	public void j_ai_fini_de_verifier_l_UPR_rentree_par_l_utilisateur() {
	    addooPage.j_ai_fini_de_verifier_l_UPR_rentree_par_l_utilisateur();
	}
	@When("^\\[ADDOO\\]Je clique sur le bouton Valider UPR$")
	public void je_clique_sur_le_bouton_Valider_UPR() {
	    addooPage.je_clique_sur_le_bouton_Valider_UPR();
	}
	@Then("^\\[ADDOO\\]La checkbox Valider UPR de la partie operation a ete cochee$")
	public void la_checkbox_Valider_UPR_de_la_partie_operation_a_ete_cochee() {
	    addooPage.la_checkbox_Valider_UPR_de_la_partie_operation_a_ete_cochee();
	}

/**Scenario: [ADDOO]
* WIP
* Valider le siege de la demande d'amenagement creee
*/
	
	@Given("^\\[ADDOO\\]Une demande d'amenagement a ete creee et l'utilisateur vient de valider l'UPR$")
	public void une_demande_d_amenagement_a_ete_creee_et_l_utilisateur_vient_de_valider_l_UPR() {
	    addooPage.une_demande_d_amenagement_a_ete_creee_et_l_utilisateur_vient_de_valider_l_UPR();
	}
	@When("^\\[ADDOO\\]J'ai fini de verifier le siege rentree par l'utilisateur$")
	public void j_ai_fini_de_verifier_le_siege_rentree_par_l_utilisateur() {
	   addooPage.j_ai_fini_de_verifier_le_siege_rentree_par_l_utilisateur();
	}
	@When("^\\[ADDOO\\]Je clique sur le bouton Valider siege$")
	public void addoo_Je_clique_sur_le_bouton_Valider_siege() {
	    addooPage.addoo_Je_clique_sur_le_bouton_Valider_siege();
	}
	@Then("^\\[ADDOO\\]La checkbox Valider siege de la partie operation a ete cochee$")
	public void addoo_La_checkbox_Valider_siege_de_la_partie_operation_a_ete_cochee() {
		addooPage.addoo_La_checkbox_Valider_siege_de_la_partie_operation_a_ete_cochee();
	}

/**Scenario: [ADDOO]
* WIP
* Traiter une demande en situation d'erreur
*/
	
	@Given("^\\[ADDOO]\\Une demande de la part de ADDOO a ete rejetee par NORIA$")
	public void addoo_Une_demande_de_la_part_de_addoo_a_ete_rejetee_par_noria() {	
		addooPage.addoo_Une_demande_de_la_part_de_addoo_a_ete_rejetee_par_noria();
	}
	@And("^\\[ADDOO]\\Un message d erreur est indique dans l onglet Demandes en erreur$")
	public void un_message_d_erreur_est_indique_dans_l_onglet_demandes_en_erreur() {
		addooPage.un_message_d_erreur_est_indique_dans_l_onglet_demandes_en_erreur();
	}
	@Then("^\\[ADDOO]\\Je change les valeurs correspondantes sur l'interface de demande pour traiter l erreur$")
	public void je_change_les_valeurs_correspondantes_sur_l_interface_de_demande_pour_traiter_l_erreur() {
		addooPage.je_change_les_valeurs_correspondantes_sur_l_interface_de_demande_pour_traiter_l_erreur();
	}
	@Then("^\\[ADDOO]\\J enregistre les nouvelles valeurs en cliquant sur Enregistrer$")
	public void j_enregistre_les_nouvelles_valeurs_en_cliquant_sur_enregistrer() {
		addooPage.j_enregistre_les_nouvelles_valeurs_en_cliquant_sur_enregistrer();
	}
	@And("^\\[ADDOO]\\J annule la validation de siege$")
	public void j_annule_la_validation_de_siege() {
		addooPage.j_annule_la_validation_de_siege();
	}
	@And("^\\[ADDOO]\\J annule la validation d UPR$")
	public void j_annule_la_validation_d_upr() {
		addooPage.j_annule_la_validation_d_upr();
	}
	@And("^\\[ADDOO]\\Je valide de nouveau l UPR$")
	public void je_valide_de_nouveau_l_upr() {
		addooPage.je_valide_de_nouveau_l_upr();
	}
	@And("^\\[ADDOO]\\Je valide de nouveau le siege$")
	public void je_valide_de_nouveau_le_siege() {
		addooPage.je_valide_de_nouveau_le_siege();
	}
	@Then("^\\[ADDOO]\\La demande ne doit plus etre en erreur$")
	public void la_demande_ne_doit_plus_etre_en_erreur() {
		addooPage.la_demande_ne_doit_plus_etre_en_erreur();
	}
}
