Feature: Amenagement de site - Interface NORIA
[NORIA]Afin de repondre a la demande du client
[NORIA]En tant qu'utilisateur final reel
[NORIA]Je veux avoir la possibilite d'amenager un site tout en prenant en compte les contraintes metiers


Scenario: [NORIA]Acceder a NORIA et rechercher une demande ADDOO
Given [NORIA]Je suis sur la page d'accueil
When [NORIA]J'accede a la partie deploiement de NORIA en cliquant sur l'onglet deploiement
And [NORIA]Je recherche une demande a partir de son numero d'identification defini dans ADDOO lors de sa creation
And [NORIA]Je clique sur le bouton rechercher
And [NORIA]Je selectionne la demande et je clique sur initialiser
Then [NORIA]L'utilisateur accede a une nouvelle page Initialisation d'operation

#5 à 6 minutes
Scenario: [NORIA]Creer un site theorique
Given [NORIA]Je me trouve sur la page d'Initialisation d'operation
When [NORIA]Je selectionne le code PRG
And [NORIA]Je clique sur le bouton creer sur le site theorique
And [NORIA]Je remplis le champ adresse
And [NORIA]Je clique sur le bouton sauvegarder
Then [NORIA] Un site theorique vient d'etre cree et les informations relatives au site sont mises a jour dans la page principale de l'Initialisation d'operation


Scenario: [NORIA]Modifier la valeur en Z d'un site theorique cree
Given [NORIA]Je me trouve sur la page d'Initialisation d'operation et l'utilisateur vient de creer un site theorique
When [NORIA]Je clique sur le bouton modifier le site theorique
And [NORIA]Je modifie la valeur en Z du site theorique
And [NORIA]Je clique sur le bouton sauvegarder afin de valider la modification de la valeur en Z
Then [NORIA]La valeur en Z du site theorique a ete modifiee et sa modification est visible sur la page d'Initialisation d'operation


Scenario: [NORIA]Etendre le rayon de recherche d'un site pour l'operation en cours 
Given [NORIA]Je viens de creer et modifier un site theorique
And [NORIA]J'accede a la page permettant le traitement d'une operation en cliquant sur le bouton valider
When [NORIA]Je clique sur le bouton details de l'operation
And [NORIA]Je modifie le rayon de recherche
And [NORIA]Je valide ma saisie en cliquant sur le bouton sauvegarder
Then [NORIA]L'utilisateur revient sur la page traitement d'operation avec le rayon de recherche modifie

#6:40 à 6:49 minutes
Scenario: [NORIA]Ajouter des element(s) de reseau(x) (Network Element)
Given [NORIA]Je suis sur la page permettant de traiter une operation et je viens de modifier le rayon de recherche du site theorique
When [NORIA]Je clique sur le bouton ajouter au niveau des elements de reseau
And [NORIA]Je clique sur sauvegarder en ne modifiant aucune valeur
Then [NORIA]Un element de reseau vient d'etre cree et celui-ci est visible avec un fond orange

#6:54 à 7:15 minutes
Scenario: [NORIA]Ajouter de(s) noeud(s) cellulaire(s)
Given [NORIA]Je viens d'ajouter un element de reseau 
When [NORIA]Je clique sur le bouton ajouter au niveau des noeuds cellulaires
And [NORIA]Je modifie les valeurs lies a la description du noeud cellulaire
And [NORIA]Je clique sur sauvegarder
Then [NORIA]Un noeud cellulaire vient d'etre cree et celui-ci est visible avec un fond orange


#7:50 à 8:10 minutes
Scenario: [NORIA]Valider la fiche fiche_navette_1 permettant l'amenagement
Given [NORIA]Je viens de creer un element de reseau et un noeud cellulaire
When [NORIA]Je clique sur le bouton valider FN1 dans la partie Actions sur le candidat 
And [NORIA]Je valide la date de validation
Then [NORIA]L'amenagement de l'operation a ete accepte et valide

#8:15 à 8:30 minutes
Scenario: [NORIA]Verifier que l'amenagement a ete validee sur l'interface ADDOO
Given [NORIA]Je me trouve sur l'interface ADDOO et je viens de valider une fiche navette d'amenagement pour l'operation en cours
When [NORIA]J'accede aux demandes en cliquant sur le sous-onglet Demandes integrees de l'onglet Suivi 
Then [NORIA]La demande d'amenagement nommee AMENAGEMENT_SCENARIO_UN est visible dans la liste


